
class Abortexec(Exception):
    Message = "Script aborted"

    def __init__(self, Message):
        self.Message = Message
        return

    def __str__(self):
        print(self.Message)
        return "False"

class MissingParameter(Exception):
    Message = ""

    def __init__(self, Message):
        self.Message = Message

        return

    def __str__(self):
        print(self.Message)
        return "False"
