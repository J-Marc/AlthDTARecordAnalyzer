'''
Created on 18 May 2020
@author: Jean-Marc Pascal
@contact: jean-marc.pascal@nokia.com
@organization: NOKIA

'''
#from IPython.core.debugger import set_trace
import os
import time
import numpy as np
import pandas as pd
import re
from typing import List
#import logging
import logging.config
import logging.handlers
import loggingHandlers

import common.config as cfg
from configparser import ConfigParser, ExtendedInterpolation

#logging.config.fileConfig(os.path.join(os.path.dirname( __file__ ),cfg.LOGGING_CONFIG_FILE))

#logger = logging.getLogger(__name__)

user = ConfigParser(interpolation=ExtendedInterpolation())
user.read(cfg.USER_CONFIG_FILE)

import pathlib

git_revision = "UNKNOWN"

ROOT = os.path.join(os.path.dirname( __file__ ),'..','..')


def get_git_revision(base_path):
#    logger.info("Reading Git revision from directory: " + base_path)
    git_dir = pathlib.Path(base_path) / '.git'
    with (git_dir / 'HEAD').open('r') as head:
        ref = head.readline().split(' ')[-1].strip()

    with (git_dir / ref).open('r') as git_hash:
        return git_hash.readline().strip()

git_revision = get_git_revision(ROOT)
git_revision_short = git_revision[:7]
#logger.info("Git repository revision: " + git_revision + " (short: " + git_revision_short + ")")

class InputError(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return self.msg

class InconsistentSourceError(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return self.msg


def convert_to_csv(filename):
    ''' Create CSV file from Excel XLS file
    '''
    csv_filename = os.path.splitext(filename)[0] + ".csv"
#    logger.info("Converting Excel file " + filename + " to CSV...")
    df = pd.read_excel(filename)
    df.to_csv(csv_filename, index=False)
#    logger.info(" Done")
    

if __name__ == '__main__':
    pass