import os

APP_ROOT_DIR = os.path.join(os.path.dirname( __file__ ), '..', '..')


############################
#    Data to analyse       #
############################

OPTIONAL_ZIPPED_SUBFOLDER = "Output"   # dependes how the Ouput.zip file is created

PATH_PM_TRACES_OUT = "mqtt-tl_logs\<testcase>\daikin\denv\edc\dta\docker\DTA_Daikin_FunctionalNode_*_PLANT_MODEL*\out\physics"
PATH_PM_TRACES_IN = "mqtt-tl_logs\<testcase>\daikin\denv\edc\dta\docker\DTA_Daikin_FunctionalNode_*_PLANT_MODEL*\in\physics"
# examples for <testcase>
#     Basic_check_normal_heating_and_buffering_for_DHW
#     Basic_check_normal_heating_and_buffering_for_Space

PATH_ALTH3_LOGS = "logs"

PM_LOGFILE_NAME = "log*.txt"
MMI_RAM_LOGFILE_NAME = "MMI_RAM*.csv"
HYDRO_RAM_LOGFILE_NAME = "RAMHydro*.csv"
OU_RAM_LOGFILE_NAME = "RAMOU*.csv"

######################
#  user config keys  #
######################
PM_IN_LOG_KEY = "Plant model input parameters".lower()
PM_OUT_LOG_KEY = "Plant model output parameters".lower()
OU_LOG_KEY = "Outdoor unit parameters".lower()
IU_LOG_KEY = "Indoor unit parameters".lower()
MMI_LOG_KEY = "MMI parameters".lower()


####################
#  Error handling  #
####################

ERROR_CODES = { 99: "Parameter not found in log",
               100: "Too many MQTT log files for plant model OUT parameters",
               101: "Too many MQTT log files for plant model IN parameters",
               102: "Too many log files for Hydro RAM parameters",
               103: "Too many log files for OU RAM parameters",
               104: "Too many log files for MMI RAM parameters",
               105: "MQTT log files for plant model OUT parameters not found",
               106: "MQTT log files for plant model IN parameters not found",
               107: "Log files for Hydro RAM parameters not found",
               108: "Log files for OU RAM parameters not found",
               109: "Log files for MMI RAM parameters not found"}

##############################
#  Storage analysis results  #
##############################


ANALYSIS_DIR = os.path.join(APP_ROOT_DIR, 'Analysis')

USER_CONFIG_FILE = os.path.join(APP_ROOT_DIR, 'src', 'user_config.ini')

LOGGING_CONFIG_FILE = 'logging.ini'
LOGGING_DIR         = 'logs'                # default is 'logs' if not specified, TO specify current directory , use '.'
LOGGING_FILE        = 'traces.log'


if __name__ == '__main__':
    pass
