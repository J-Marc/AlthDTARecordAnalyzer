import sys
sys.path.append('common')   # to be able to run from command line, to find the python modules in this subdirectory
                            # otherwie should add it to the Windows System PATH

import argparse
import matplotlib.pyplot as plt
plt.rcParams['axes.grid'] = True
from zipfile import ZipFile

from src.common import config as cfg
from common.common import *

from common.error_handling import *

logger = logging.getLogger(__name__)

class GenerateGraphs:
    def insert_relative_time(self):
        if self.plant_model_logs:
            self.df_log[cfg.PM_OUT_LOG_KEY]["relative_time_s"] = (self.df_log[cfg.PM_OUT_LOG_KEY]["time_ms"] - self.df_log[cfg.PM_OUT_LOG_KEY].loc[0, "time_ms"]) / 1000 - 5*60 + 23
            self.df_log[cfg.PM_IN_LOG_KEY]["relative_time_s"] = (self.df_log[cfg.PM_IN_LOG_KEY]["time_ms"] - self.df_log[cfg.PM_IN_LOG_KEY].loc[0, "time_ms"]) / 1000 - 5*60 + 26

        self.df_log[cfg.IU_LOG_KEY]["Time"] = pd.to_datetime(self.df_log[cfg.IU_LOG_KEY]['Time'], format='%Y-%m-%d_%H:%M:%S.%f')
        difference = self.df_log[cfg.IU_LOG_KEY]["Time"] - self.df_log[cfg.IU_LOG_KEY].loc[0, "Time"]
        self.df_log[cfg.IU_LOG_KEY]["relative_time_s"] = difference.dt.total_seconds()

        self.df_log[cfg.OU_LOG_KEY]["Time"] = pd.to_datetime(self.df_log[cfg.OU_LOG_KEY]['Time'], format='%Y-%m-%d_%H:%M:%S.%f')
        difference = self.df_log[cfg.OU_LOG_KEY]["Time"] - self.df_log[cfg.OU_LOG_KEY].loc[0, "Time"]
        self.df_log[cfg.OU_LOG_KEY]["relative_time_s"] = difference.dt.total_seconds()

        self.df_log[cfg.MMI_LOG_KEY]["Time"] = pd.to_datetime(self.df_log[cfg.MMI_LOG_KEY]['Time'], format='%Y-%m-%d_%H:%M:%S.%f')
        difference = self.df_log[cfg.MMI_LOG_KEY]["Time"] - self.df_log[cfg.MMI_LOG_KEY].loc[0, "Time"]
        self.df_log[cfg.MMI_LOG_KEY]["relative_time_s"] = difference.dt.total_seconds()

    def Plot_refrigerant_pressure(self):
        if not self.df_log[cfg.PM_OUT_LOG_KEY].empty:
            self.df_log[cfg.PM_OUT_LOG_KEY]["computed_refrigerant_pressure"] = np.exp((self.df_log[cfg.PM_OUT_LOG_KEY]["drefrigerant"] - 10.784) / 51.172)
            self.df_log[cfg.PM_OUT_LOG_KEY].plot("relative_time_s", "drefrigerant")
            #self.df_log[cfg.PM_OUT_LOG_KEY].plot("relative_time_s", "drefrigerant_pressure")
            self.df_log[cfg.PM_OUT_LOG_KEY].plot("relative_time_s", "computed_refrigerant_pressure")

    def Plot_Plant_model_power_converter(self):
        if  not self.df_log[cfg.PM_IN_LOG_KEY].empty:
            self.df_log[cfg.PM_IN_LOG_KEY].plot("doutdoor")
            self.df_log[cfg.PM_IN_LOG_KEY][my_graph.df_pm_in['ucompressor_speed'].notna()].plot("relative_time_s", "ucompressor_speed")
            self.df_log[cfg.PM_OUT_LOG_KEY].plot("relative_time_s", "qheatpump_current")
            self.df_log[cfg.PM_OUT_LOG_KEY].plot("relative_time_s", "droom")
        self.df_log[cfg.OU_LOG_KEY].plot("relative_time_s", "Current_freq [None]")
        self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "BUH_relay_1 [None]")
        self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "BUH_relay_2 [None]")

    def Plot_OU_current_consumption(self):
        self.df_log[cfg.OU_LOG_KEY].plot("relative_time_s", "Pc(HP)_GI [bar]")

    def Plot_MMI_parameters(self):
        self.df_log[cfg.MMI_LOG_KEY].plot("relative_time_s", "HP_elec_input_kw [kW]")
        self.df_log[cfg.MMI_LOG_KEY].plot("relative_time_s", "System_elec_input_kW [kW]")
        self.df_log[cfg.MMI_LOG_KEY].plot("relative_time_s", "Limit_setting_kw [kW]")
        self.df_log[cfg.MMI_LOG_KEY].plot("relative_time_s", "HH_pGrid [A]")

    def Plot_Hydro_errors(self):
        self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "Error_code [None]")

    def Plot_power_consumption(self):
        self.Plot_Plant_model_power_converter()
        self.df_log[cfg.OU_LOG_KEY].plot("relative_time_s", "dbg_INV_First_Current [A]")
        if  "WHP2 [None]" in self.df_log[cfg.IU_LOG_KEY].index: self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "WHP2 [None]")
        if "HP_elec_input_kw/4 [None]" in self.df_log[cfg.IU_LOG_KEY].index: self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "HP_elec_input_kw/4 [None]")
        self.df_log[cfg.MMI_LOG_KEY].plot("relative_time_s", "HP_elec_input_kw [kW]")
        if  not self.df_log[cfg.PM_IN_LOG_KEY].empty:
            self.df_log[cfg.PM_OUT_LOG_KEY].plot("relative_time_s", "dunit_entering")
            self.df_log[cfg.PM_IN_LOG_KEY][my_graph.df_pm_in['bcompressor'].notna()].plot("relative_time_s", "bcompressor")

    def Plot_inlet_oulet_water_temperature(self):
        self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "DLWA [C]")
    def Plot_Space_Heating_issue(self):
        self.df_log[cfg.PM_IN_LOG_KEY][my_graph.df_pm_in['ucompressor_speed'].notna()].plot("relative_time_s", "ucompressor_speed")
        self.Plot_inlet_oulet_water_temperature()

    def Plot_Pump_speed_conversion(self):
        #self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "Control_PWM")
        if not self.df_log[cfg.PM_IN_LOG_KEY].empty:
            self.df_log[cfg.PM_IN_LOG_KEY][my_graph.df_pm_in['upump_speed'].notna()].plot("relative_time_s", "upump_speed")

    def Plot_heating_operation(self):
        self.Plot_inlet_oulet_water_temperature()
        self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "RT_Temp [C]")
        self.Plot_OU_current_consumption()
        if "WHP2 [None]" in self.df_log[cfg.IU_LOG_KEY].index: self.df_log[cfg.IU_LOG_KEY].plot("relative_time_s", "WHP2 [None]")
        self.df_log[cfg.MMI_LOG_KEY].plot("relative_time_s", "HP_elec_input_kw [kW]")

    def Plot_readout_inverter_currents(self):
        self.df_log[cfg.OU_LOG_KEY].plot("relative_time_s", "INV_first_Current [None]")
        self.df_log[cfg.OU_LOG_KEY].plot("relative_time_s", "INV_second_Current [None]")

class GraphsFromLogfiles(GenerateGraphs):
    """Class extracting Altherma 3 recorded data from a folder with log files
    INCOMPLETE
    """
    def __init__(self, in_csv_file, out_csv_file):
        self.df_log[cfg.PM_OUT_LOG_KEY] = pd.read_csv(out_csv_file)
        self.df_log[cfg.PM_IN_LOG_KEY] = pd.read_csv(in_csv_file)

        self.insert_relative_time()

class GraphsFromZip(GenerateGraphs):
    """Class extracting Altherma 3 recorded data from a ZIP archive of log files"""
    def __init__(self, zipfile, plant_model_logs):

        self.zipfile = zipfile
        self.plant_model_logs = plant_model_logs
        self.df_log = dict()

        if self.plant_model_logs:
            cfg.PATH_PM_TRACES_OUT = cfg.PATH_PM_TRACES_OUT.replace("<testcase>", args.testcase)
            print(cfg.PATH_PM_TRACES_OUT)
            cfg.PATH_PM_TRACES_IN = cfg.PATH_PM_TRACES_IN.replace("<testcase>", args.testcase)
            print(cfg.PATH_PM_TRACES_IN)

            self.df_log[cfg.PM_OUT_LOG_KEY] = self.__find_matching_logfile(cfg.PATH_PM_TRACES_OUT, cfg.PM_LOGFILE_NAME, cfg.ERROR_CODES[100], cfg.ERROR_CODES[105])
            print(list(self.df_log[cfg.PM_OUT_LOG_KEY].keys()))
            print(self.df_log[cfg.PM_OUT_LOG_KEY].head())

            self.df_log[cfg.PM_IN_LOG_KEY] = self.__find_matching_logfile(cfg.PATH_PM_TRACES_IN, cfg.PM_LOGFILE_NAME, cfg.ERROR_CODES[101], cfg.ERROR_CODES[106])
            print(list(self.df_log[cfg.PM_IN_LOG_KEY].keys()))
            print(self.df_log[cfg.PM_IN_LOG_KEY].head())
        else:
            self.df_log[cfg.PM_OUT_LOG_KEY] = pd.DataFrame() # creates a new emptydataframe
            self.df_log[cfg.PM_IN_LOG_KEY] = pd.DataFrame() # creates a new emptydataframe

        self.df_log[cfg.IU_LOG_KEY] = self.__find_matching_logfile(cfg.PATH_ALTH3_LOGS, cfg.HYDRO_RAM_LOGFILE_NAME, cfg.ERROR_CODES[102], cfg.ERROR_CODES[107])
        print(list(self.df_log[cfg.IU_LOG_KEY].keys()))
        print(self.df_log[cfg.IU_LOG_KEY].head())

        self.df_log[cfg.OU_LOG_KEY] = self.__find_matching_logfile(cfg.PATH_ALTH3_LOGS, cfg.OU_RAM_LOGFILE_NAME, cfg.ERROR_CODES[103], cfg.ERROR_CODES[108])
        print(list(self.df_log[cfg.OU_LOG_KEY].keys()))
        print(self.df_log[cfg.OU_LOG_KEY].head())

        self.df_log[cfg.MMI_LOG_KEY] = self.__find_matching_logfile(cfg.PATH_ALTH3_LOGS, cfg.MMI_RAM_LOGFILE_NAME, cfg.ERROR_CODES[104], cfg.ERROR_CODES[109])
        print(list(self.df_log[cfg.MMI_LOG_KEY].keys()))
        print(self.df_log[cfg.MMI_LOG_KEY].head())

        self.insert_relative_time()
    def __find_matching_logfile(self, filepath, filename, error_too_many, error_not_found):
        pattern_to_search = filepath + "\\" + filename
        pattern_to_search = os.path.normpath(pattern_to_search).replace('\\', '/')

        zip_content = self.zipfile.namelist()

        # convert the search pattern to regex:
        pattern_to_search = pattern_to_search.replace('.', '\.')
        pattern_to_search = pattern_to_search.replace('*', '.*')
        pattern_to_search = "(" + cfg.OPTIONAL_ZIPPED_SUBFOLDER + "/" + ")?" + pattern_to_search

        print('pattern to search for:')
        print(pattern_to_search)
        regex_pattern = re.compile(pattern_to_search)
        matching_filelist = list(filter(regex_pattern.match, zip_content))

        print(matching_filelist)
        if len(matching_filelist) > 1: raise Abortexec(error_too_many)
        if len(matching_filelist) < 1: raise Abortexec(error_not_found)

        df_content = pd.read_csv(self.zipfile.open(matching_filelist[0], 'r'))
        return df_content

    def plot_parameter(self, log_key, log_parameter):
        print('log_key: ', log_key, ' | ',  'log_parameter: ', log_parameter)
        if log_parameter in self.df_log[log_key].keys():
            self.df_log[log_key][log_parameter] = self.df_log[log_key][log_parameter].interpolate(method='linear')
            self.df_log[log_key].plot("relative_time_s", log_parameter)
        else:
            raise MissingParameter(cfg.ERROR_CODES[99] + ": " + log_parameter + ' in ' + log_key)


    def create_all_graphs_from_config_file(self):
        print('list of keys:', list(user[self.section].keys()))
        for log_type in user[self.section].keys():
            parameters_list = user[self.section][log_type].split('\n')
            parameters_list = [x for x in parameters_list if x]
            print("-" * 20)
            print(log_type)
            print("-" * 20)
            for parameter in parameters_list:
                self.plot_parameter(log_type, parameter)

    def Space_Heating_diagnostics(self):
        self.section = 'Space Heating diagnostics'
        self.create_all_graphs_from_config_file()

    def investigate_Altherma_reported_HP_power_consumption(self):
        self.section = 'investigate Altherma reported HP power consumption'
        self.create_all_graphs_from_config_file()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Analyses logs collected by E2E SW test in DTA framework.')
    parser.add_argument('zipfile', metavar='ZIPFILENAME', help='the OUTPUT logfile name as ZIP archive')
    parser.add_argument('testcase', metavar='TESTCASENAME', help='The test case name')
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    parser.add_argument('--no_plant_model_logs', action='store_true', help='indicates there are not Plant model MQTT logs to extract')

    args = parser.parse_args()
    print(args)
    my_graph = GraphsFromZip(ZipFile(args.zipfile, 'r'), not(args.no_plant_model_logs))

    #my_graph.Space_Heating_diagnostics()
    my_graph.investigate_Altherma_reported_HP_power_consumption()

    # my_graph.Plot_Hydro_errors()
    # #my_graph.Plot_Space_Heating_issue()
    #
    # my_graph.Plot_refrigerant_pressure()
    #
    # my_graph.Plot_Pump_speed_conversion()
    # #my_graph.Plot_Plant_model_power_converter()
    # #
    # # my_graph.Plot_OU_current_consumption()
    # #
    # # my_graph.Plot_MMI_parameters()
    #
    # my_graph.Plot_power_consumption()
    #
    # #my_graph.Plot_heat_production_actuators()
    #
    # my_graph.Plot_heating_operation()
    # my_graph.Plot_inlet_oulet_water_temperature()
    # my_graph.Plot_readout_inverter_currents()

    plt.show()
